# Matrix Transposition

| Repo | Link |
| --- | --- |
| Skeleton | https://gitlab.com/autocode-exercises/java-arrays/transpose-matrix |
| Solution | https://gitlab.com/autocode-exercises-solution/java-arrays/transpose-matrix |
