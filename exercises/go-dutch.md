# Go Dutch

| Repo | Link |
| --- | --- |
| Skeleton | https://gitlab.com/autocode-exercises/java-basics/go-dutch |
| Solution | https://gitlab.com/autocode-exercises-solution/java-basics/go-dutch |
