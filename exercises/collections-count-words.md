# Collections. Count Words.
Purpose of this exercise is to develop an ability to choose the proper collection from Java Collections Framework in order to solve different types of problems.
You will have to collect your data in some collection of your choice and process it using provided by framework methods

| Repo | Link |
| --- | --- |
| Skeleton | https://gitlab.com/upskill-java/pro-javase/collections-count-words <br/> https://gitlab.com/efimchik-autocode-tasks/java-extended/collections-count-words |
| Solution | https://gitlab.com/upskill-java/pro-javase/collections-count-words-solution |

