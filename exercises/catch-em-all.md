# Catch'em all

Purpose of this exercise is to train you to handle exceptions.

| Repo | Link |
| --- | --- |
| Skeleton | https://gitlab.com/autocode-exercises/java-exceptions/catch-em-all |
| Skeleton | https://gitlab.com/upskill-java/deep-dive/catch-em-all |
| Skeleton | https://gitlab.com/efimchik-autocode-tasks/java-basics/catch-em-all |
| Solution | https://gitlab.com/autocode-exercises-solution/java-exceptions/catch-em-all |
| Solution | https://gitlab.com/upskill-java/deep-dive/catch-em-all-solution |
| Solution | https://gitlab.com/efimchik-autocode-tasks/java-basics/catch-em-all-solution |

