# Spring Chess Puzzles XML

Purpose of this exercise is to train you to define beans. You may benefit from dynamic bean definition approach.

| Repo | Link |
| --- | --- |
| Skeleton | https://gitlab.com/upskill-java/spring/chess-puzzles-xml |
| Solution | https://gitlab.com/upskill-java/spring/chess-puzzles-xml-solution |

