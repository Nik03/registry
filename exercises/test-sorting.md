# Test Sorting

Purpose of this exercise is to train you to design and create unit tests via JUnit 4.

| Repo | Link |
| --- | --- |
| Skeleton | https://gitlab.com/upskill-java/deep-dive/test-sorting <br/> https://gitlab.com/efimchik-autocode-tasks/junit/test-sorting |
| Solution | https://gitlab.com/upskill-java/deep-dive/test-sorting-solution <br/> https://gitlab.com/efimchik-autocode-tasks/junit/test-sorting-solution |

