# Service

Purpose of this exercise is to train you to implement Service layer of an application. 

| Repo | Link |
| --- | --- |
| Skeleton | https://gitlab.com/efimchik-autocode-tasks/sql-jdbc/service |
| Solution | https://gitlab.com/efimchik-autocode-tasks/sql-jdbc/service-solution |

