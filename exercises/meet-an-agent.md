# Meet an Agent

| Repo | Link |
| --- | --- |
| Skeleton | https://gitlab.com/autocode-exercises/java-basics/meet-an-agent |
| Solution | https://gitlab.com/autocode-exercises-solution/java-basics/meet-an-agent |
