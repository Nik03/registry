# Max Value in Sequence

| Repo | Link |
| --- | --- |
| Skeleton | https://gitlab.com/autocode-exercises/java-basics/max-value-in-sequence |
| Solution | https://gitlab.com/autocode-exercises-solution/java-basics/max-value-in-sequence |
