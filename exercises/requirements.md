# Requirements

| Repo | Link |
| --- | --- |
| Skeleton | https://gitlab.com/autocode-exercises/java-exceptions/requirements |
| Solution | https://gitlab.com/autocode-exercises-solution/java-exceptions/requirements |