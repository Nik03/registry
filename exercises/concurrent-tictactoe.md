# Concurrent TicTacToe

| Repo | Link |
| --- | --- |
| Skeleton | https://gitlab.com/upskill-java/pro-javase/concurrent-tic-tac-toe <br/> https://gitlab.com/efimchik-autocode-tasks/java-extended/concurrent-tic-tac-toe |
| Solution | https://gitlab.com/upskill-java/pro-javase/concurrent-tic-tac-toe-solution <br/> https://gitlab.com/efimchik-autocode-tasks/java-extended/concurrent-tic-tac-toe-solution |

