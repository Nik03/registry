# Meet Autocode

This exercise will make you familiar with Autocode platform.

| Repo | Link |
| --- | --- |
| Skeleton | https://gitlab.com/autocode-exercises/java-basics/meet-autocode |
| Skeleton | https://gitlab.com/upskill-java/deep-dive/meet-autocode|
| Skeleton | https://gitlab.com/efimchik-autocode-tasks/java-basics/meet-autocode |
| Solution | https://gitlab.com/autocode-exercises-solution/java-basics/meet-autocode |
| Solution | https://gitlab.com/upskill-java/deep-dive/meet-autocode-solution |
| Solution | https://gitlab.com/efimchik-autocode-tasks/java-basics/meet-autocode-solution |


