# Jsp Currencies

Purpose of this exercise is to train you to use JSP files to provide dynamic Web interface. 

| Repo | Link |
| --- | --- |
| Skeleton | https://gitlab.com/efimchik-autocode-tasks/servlets/jsp-currencies |
| Solution | https://gitlab.com/efimchik-autocode-tasks/servlets/jsp-currencies-solution |

