# Average

Purpose of this exercise is to train you working with cycles to solve a regular math problem.

| Repo | Link |
| --- | --- |
| Skeleton | https://gitlab.com/autocode-exercises/java-basics/average |
| Skeleton | https://gitlab.com/upskill-java/deep-dive/average |
| Skeleton | https://gitlab.com/efimchik-autocode-tasks/java-basics/average |
| Solution | https://gitlab.com/autocode-exercises-solution/java-basics/average |
| Solution | https://gitlab.com/upskill-java/deep-dive/average-solution |
| Solution | https://gitlab.com/efimchik-autocode-tasks/java-basics/average-solution |

