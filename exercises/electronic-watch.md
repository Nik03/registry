# Electronic Watch

Purpose of this exercise is to train you working with integer operations.

| Repo | Link |
| --- | --- |
| Skeleton | https://gitlab.com/autocode-exercises/java-basics/electronic-watch |
| Skeleton | https://gitlab.com/upskill-java/deep-dive/electronic-watch |
| Skeleton | https://gitlab.com/efimchik-autocode-tasks/java-basics/electronic-watch |
| Solution | https://gitlab.com/autocode-exercises-solution/java-basics/electronic-watch |
| Solution | https://gitlab.com/upskill-java/deep-dive/electronic-watch-solution |
| Solution | https://gitlab.com/efimchik-autocode-tasks/java-basics/electronic-watch-solution |

