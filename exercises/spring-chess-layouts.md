# Spring Chess Layouts

Purpose of this exercise is to train you to define beans. You may benefit from dynamic bean definition approach. 

| Repo | Link |
| --- | --- |
| Skeleton | https://gitlab.com/efimchik-autocode-tasks/spring-core/spring-chess-layouts |
| Solution | https://gitlab.com/efimchik-autocode-tasks/spring-core/spring-chess-layouts-solution |

