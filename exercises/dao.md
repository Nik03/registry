# DAO

Purpose of this exercise is to train you to employ DAO pattern to design data access layer of an application. 

| Repo | Link |
| --- | --- |
| Skeleton | https://gitlab.com/efimchik-autocode-tasks/sql-jdbc/dao |
| Solution | https://gitlab.com/efimchik-autocode-tasks/sql-jdbc/dao-solution |

