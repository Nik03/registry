# Expression Calculator

Purpose of this exercise is to train you to handle http requests with Servlets

| Repo | Link |
| --- | --- |
| Skeleton | https://gitlab.com/efimchik-autocode-tasks/servlets/expression-calculator |
| Solution | https://gitlab.com/efimchik-autocode-tasks/servlets/expression-calculator-solution |

