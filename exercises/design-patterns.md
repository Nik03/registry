# Design Patterns
Purpose of this exercise is to train thinking with design patterns and get hands on experience with them.

| Repo | Link |
| --- | --- |
| Skeleton | https://gitlab.com/upskill-java/pro-javase/design-patterns <br/> https://gitlab.com/efimchik-autocode-tasks/java-extended/design-patterns|
| Solution | https://gitlab.com/upskill-java/pro-javase/design-patterns-solution <br/> https://gitlab.com/efimchik-autocode-tasks/java-extended/design-patterns-solution |
