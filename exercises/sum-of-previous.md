# Sum of Previous

| Repo | Link |
| --- | --- |
| Skeleton | https://gitlab.com/autocode-exercises/java-arrays/sum-of-previous |
| Solution | https://gitlab.com/autocode-exercises-solution/java-arrays/sum-of-previous |
