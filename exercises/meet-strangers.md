# Meet Strangers

| Repo | Link |
| --- | --- |
| Skeleton | https://gitlab.com/autocode-exercises/java-basics/meet-strangers |
| Solution | https://gitlab.com/autocode-exercises-solution/java-basics/meet-strangers |
