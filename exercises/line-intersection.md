# Line Intersection

| Repo | Link |
| --- | --- |
| Skeleton | https://gitlab.com/autocode-exercises/java-oop/line-intersection |
| Solution | https://gitlab.com/autocode-exercises-solution/java-oop/line-intersection |
