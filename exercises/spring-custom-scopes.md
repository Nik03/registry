# Spring Custom Scopes

Purpose of this exercise is to train you to use post processors to define and use custom beans. 

| Repo | Link |
| --- | --- |
| Skeleton | https://gitlab.com/efimchik-autocode-tasks/spring-core/spring-custom-scopes |
| Skeleton | https://gitlab.com/upskill-java/spring/spring-custom-scopes |
| Solution | https://gitlab.com/efimchik-autocode-tasks/spring-core/spring-custom-scopes-solution |
| Solution | https://gitlab.com/upskill-java/spring/spring-custom-scopes-solution |

