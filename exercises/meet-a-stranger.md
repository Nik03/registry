# Meet a Stranger

| Repo | Link |
| --- | --- |
| Skeleton | https://gitlab.com/autocode-exercises/java-basics/meet-a-stranger |
| Solution | https://gitlab.com/autocode-exercises-solution/java-basics/meet-a-stranger.git |
