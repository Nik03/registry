# Figures. Extra Challenge

Purpose of this exercise is to train you to design classes that extends other classes and inherit their behavior. This
exercise contains extra challenges for those who is ready for more.

| Repo | Link |
| --- | --- |
| Skeleton | https://gitlab.com/autocode-exercises/java-oop/figures-extra |
| Skeleton | https://gitlab.com/upskill-java/deep-dive/figures-extra |
| Skeleton | https://gitlab.com/efimchik-autocode-tasks/java-basics/figures-extra |
| Solution | https://gitlab.com/autocode-exercises-solution/java-oop/figures-extra |
| Solution | https://gitlab.com/upskill-java/deep-dive/figures-extra-solution |
| Solution | https://gitlab.com/efimchik-autocode-tasks/java-basics/figures-extra-solution |

