# Spring Catalog Access

Purpose of this exercise is to train you to use Spring Security to manage web app access. 

| Repo | Link |
| --- | --- |
| Skeleton | https://gitlab.com/efimchik-autocode-tasks/spring-mvc/spring-catalog-access |
| Skeleton | https://gitlab.com/upskill-java/spring/spring-catalog-access |
| Solution | https://gitlab.com/efimchik-autocode-tasks/spring-mvc/spring-catalog-access-solution |
| Solution | https://gitlab.com/upskill-java/spring/spring-catalog-access-solution |

