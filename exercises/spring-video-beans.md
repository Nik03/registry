# Spring Video Beans

Purpose of this exercise is to train you to define and wire different types of beans. 

| Repo | Link |
| --- | --- |
| Skeleton | https://gitlab.com/efimchik-autocode-tasks/spring-core/spring-video-beans |
| Skeleton | https://gitlab.com/upskill-java/spring/spring-video-beans |
| Solution | https://gitlab.com/efimchik-autocode-tasks/spring-core/spring-video-beans-solution |
| Solution | https://gitlab.com/upskill-java/spring/spring-video-beans-solution |

