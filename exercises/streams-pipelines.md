# Streams. Pipelines.
Purpose of this exercise is to train you working with Pipelines combining Non-Terminal Operations and Terminal Operation

| Repo | Link |
| --- | --- |
| Skeleton | https://gitlab.com/upskill-java/pro-javase/streams-pipelines <br/> https://gitlab.com/efimchik-autocode-tasks/java-extended/streams-pipelines |
| Solution | https://gitlab.com/upskill-java/pro-javase/streams-pipelines-solution |

