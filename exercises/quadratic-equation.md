# Quadratic Equation

Purpose of this exercise is to train you working with conditional statements to solve a regular math problem.

| Repo | Link |
| --- | --- |
| Skeleton | https://gitlab.com/autocode-exercises/java-basics/quadratic-equation |
| Skeleton | https://gitlab.com/upskill-java/deep-dive/quadratic-equation |
| Skeleton | https://gitlab.com/efimchik-autocode-tasks/java-basics/quadratic-equation |
| Solution | https://gitlab.com/autocode-exercises-solution/java-basics/quadratic-equation |
| Solution | https://gitlab.com/upskill-java/deep-dive/quadratic-equation-solution |
| Solution | https://gitlab.com/efimchik-autocode-tasks/java-basics/quadratic-equation-solution |

