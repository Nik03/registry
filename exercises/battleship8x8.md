# Battleship 8x8

| Repo | Link |
| --- | --- |
| Skeleton | https://gitlab.com/autocode-exercises/java-basics/battleship8x8 |
| Solution | https://gitlab.com/autocode-exercises-solution/java-basics/battleship8x8 |