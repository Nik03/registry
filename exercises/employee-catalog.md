# Employee Catalog

Purpose of this exercise is to train you to employ Spring MVC to design and code Web Applications that are using Databases. 

| Repo | Link |
| --- | --- |
| Skeleton | https://gitlab.com/efimchik-autocode-tasks/spring-mvc/employee-catalog |
| Skeleton | https://gitlab.com/upskill-java/spring/employee-catalog |
| Solution | https://gitlab.com/efimchik-autocode-tasks/spring-mvc/employee-catalog-solution |
| Solution | https://gitlab.com/upskill-java/spring/employee-catalog-solution |

