# Cycle Swap

Purpose of this exercise is to train you to work with arrays.

| Repo | Link |
| --- | --- |
| Skeleton | https://gitlab.com/autocode-exercises/java-arrays/cycle-swap |
| Skeleton | https://gitlab.com/upskill-java/deep-dive/cycle-swap |
| Skeleton | https://gitlab.com/efimchik-autocode-tasks/java-basics/cycle-swap |
| Solution | https://gitlab.com/autocode-exercises-solution/java-arrays/cycle-swap |
| Solution | https://gitlab.com/upskill-java/deep-dive/cycle-swap-solution |
| Solution | https://gitlab.com/efimchik-autocode-tasks/java-basics/cycle-swap-solution |

