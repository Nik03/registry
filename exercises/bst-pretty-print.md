# BST Pretty Print
Purpose of this exercise is to train you to work with advanced data structures and BST in particular

| Repo | Link |
| --- | --- |
| Skeleton | https://gitlab.com/upskill-java/pro-javase/bst-pretty-print <br/> https://gitlab.com/efimchik-autocode-tasks/java-extended/bst-pretty-print |
| Solution | https://gitlab.com/upskill-java/pro-javase/bst-pretty-print-solution <br/> https://gitlab.com/efimchik-autocode-tasks/java-extended/bst-pretty-print-solution |

