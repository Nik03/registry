# Figures

Purpose of this exercise is to train you to design classes that extends other classes and inherit their behavior.

| Repo | Link |
| --- | --- |
| Skeleton | https://gitlab.com/autocode-exercises/java-oop/figures |
| Skeleton | https://gitlab.com/upskill-java/deep-dive/figures |
| Skeleton | https://gitlab.com/efimchik-autocode-tasks/java-basics/figures |
| Solution | https://gitlab.com/autocode-exercises-solution/java-oop/figures |
| Solution | https://gitlab.com/upskill-java/deep-dive/figures-solution |
| Solution | https://gitlab.com/efimchik-autocode-tasks/java-basics/figures-solution |

