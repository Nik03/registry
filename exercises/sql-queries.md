# SQL Queries

Purpose of this exercise is to train you to use sql queries to request the needed data from relational databases. 

| Repo | Link |
| --- | --- |
| Skeleton | https://gitlab.com/efimchik-autocode-tasks/sql-jdbc/sql-queries |
| Solution | https://gitlab.com/efimchik-autocode-tasks/sql-jdbc/sql-queries-solution |

