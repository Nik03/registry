# Optional Max

| Repo | Link |
| --- | --- |
| Skeleton | https://gitlab.com/autocode-exercises/java-arrays/optional-max |
| Solution | https://gitlab.com/autocode-exercises-solution/java-arrays/optional-max |
