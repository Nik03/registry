# Sum of even numbers

| Repo | Link |
| --- | --- |
| Skeleton | https://gitlab.com/autocode-exercises/java-arrays/sum-of-even-numbers |
| Solution | https://gitlab.com/autocode-exercises-solution/java-arrays/sum-of-even-numbers |
