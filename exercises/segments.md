# Segments

Purpose of this exercise is to train you to design simple classes having state and methods.

| Repo | Link |
| --- | --- |
| Skeleton | https://gitlab.com/autocode-exercises/java-oop/segments |
| Skeleton | https://gitlab.com/upskill-java/deep-dive/segments |
| Skeleton | https://gitlab.com/efimchik-autocode-tasks/java-basics/segments |
| Solution | https://gitlab.com/autocode-exercises-solution/java-oop/segments |
| Solution | https://gitlab.com/upskill-java/deep-dive/segments-solution |
| Solution | https://gitlab.com/efimchik-autocode-tasks/java-basics/segments-solution |

