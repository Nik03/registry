# Test Quadratic Equation

Purpose of this exercise is to train you to design and create parameterized unit tests via JUnit 4

| Repo | Link |
| --- | --- |
| Skeleton | https://gitlab.com/upskill-java/deep-dive/test-quadratic-equation <br/> https://gitlab.com/efimchik-autocode-tasks/junit/test-quadratic-equation|
| Solution | https://gitlab.com/upskill-java/deep-dive/test-quadratic-equation-solution <br/> https://gitlab.com/efimchik-autocode-tasks/junit/test-quadratic-equation-solution |

