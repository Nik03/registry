# Matrices Multiplication

| Repo | Link |
| --- | --- |
| Skeleton | https://gitlab.com/autocode-exercises/java-arrays/multiply-matrices |
| Solution | https://gitlab.com/autocode-exercises-solution/java-arrays/multiply-matrices |
