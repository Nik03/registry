# Spiral

Purpose of this exercise is to train you to work with multidimensional arrays.

| Repo | Link |
| --- | --- |
| Skeleton | https://gitlab.com/autocode-exercises/java-arrays/spiral |
| Skeleton | https://gitlab.com/upskill-java/deep-dive/spiral |
| Skeleton | https://gitlab.com/efimchik-autocode-tasks/java-basics/spiral |
| Solution | https://gitlab.com/autocode-exercises-solution/java-arrays/spiral |
| Solution | https://gitlab.com/upskill-java/deep-dive/spiral-solution |
| Solution | https://gitlab.com/efimchik-autocode-tasks/java-basics/spiral-solution |

