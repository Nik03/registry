# File Tree
Purpose of this exercise is to train you working with File Tree: build a String representation of directory hierarchy under a given path

| Repo | Link |
| --- | --- |
| Skeleton | https://gitlab.com/upskill-java/pro-javase/test <br/> https://gitlab.com/efimchik-autocode-tasks/java-extended/test |
| Solution | https://gitlab.com/upskill-java/pro-javase/test-solution <br/> https://gitlab.com/efimchik-autocode-tasks/java-extended/test-solution |

