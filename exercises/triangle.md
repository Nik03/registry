# Triangle

Purpose of this exercise is to train you to design simple classes which uses composition and other classes to implement
their behavior.

| Repo | Link |
| --- | --- |
| Skeleton | https://gitlab.com/autocode-exercises/java-oop/triangle |
| Skeleton | https://gitlab.com/upskill-java/deep-dive/triangle |
| Skeleton | https://gitlab.com/efimchik-autocode-tasks/java-basics/triangle |
| Solution | https://gitlab.com/autocode-exercises-solution/java-oop/triangle |
| Solution | https://gitlab.com/upskill-java/deep-dive/triangle-solution |
| Solution | hhttps://gitlab.com/efimchik-autocode-tasks/java-basics/triangle-solution |

