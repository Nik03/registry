# Compass

| Repo | Link |
| --- | --- |
| Skeleton | https://gitlab.com/autocode-exercises/java-enums/compass |
| Solution | https://gitlab.com/autocode-exercises-solution/java-enums/compass |