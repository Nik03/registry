# RD Lab Java. Spring

## Exercises

- Meet Autocode
    - [Meet Autocode](../exercises/meet-autocode.md)
- Spring Core
    - [Spring Chess Layouts](../exercises/spring-chess-layouts.md)
    - [Spring Video Beans](../exercises/spring-video-beans.md)
    - [Spring Custom Scopes](../exercises/spring-custom-scopes.md)
- Spring MVC
    - [Spring MVC Stateful Expression Calculator](../exercises/spring-mvc-stateful-expression-calculator.md)
    - [Employee Catalog](../exercises/employee-catalog.md)
    - [Spring Catalog Access](../exercises/spring-catalog-access.md)