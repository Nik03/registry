# RD Training Test Auto Java. Standard Program SPB

## Exercises

- Meet Autocode
    - [Meet Autocode](../exercises/meet-autocode.md)
- Java Fundamentals
    - [Quadratic Equation](../exercises/quadratic-equation.md)
    - [Average](../exercises/average.md)
    - [Electronic Watch](../exercises/electronic-watch.md)
- Java Exceptions
    - [Catch'em all](../exercises/catch-em-all.md)
- Java Classes
    - [Segments](../exercises/segments.md)
    - [Triangle](../exercises/triangle.md)
- Java Collections
    - [Collections. Count Words.](../exercises/collections-count-words.md)
    - [Collections. Special Collections.](../exercises/collections-special-collections.md)
- Java Streams
    - [Streams. Count Words.](../exercises/streams-count-words.md)
- JUnit
    - [Test Sorting](../exercises/test-sorting.md)
    - [Test Quadratic Equation](../exercises/test-quadratic-equation.md)
    - [Test Factorial](../exercises/test-factorial.md)
- SQL + JDBC.
    - [Employees. SQL Queries.](../exercises/sql-queries.md)
    - [Employees. Row Mapper.](../exercises/row-mapper.md)
    - [Employees. Set Mapper.](../exercises/set-mapper.md)