# RD Training Java. Standard Program SPB

## Exercises

- Meet Autocode
    - [Meet Autocode](../exercises/meet-autocode.md)
- Java Fundamentals
    - [Quadratic Equation](../exercises/quadratic-equation.md)
    - [Average](../exercises/average.md)
    - [Electronic Watch](../exercises/electronic-watch.md)
    - [Cycle Swap](../exercises/cycle-swap.md)
    - [Spiral](../exercises/spiral.md)
- Java Classes
    - [Segments](../exercises/segments.md)
    - [Triangle](../exercises/triangle.md)
    - [Figures](../exercises/figures.md)
    - [Figures. Extra Challenge](../exercises/figures-extra-challenge.md)
- Java Exceptions
    - [Catch'em all](../exercises/catch-em-all.md)
- Java Packaging    
    - [Suffixing App](../exercises/suffixing-app.md)
- Java Collections
    - [Collections. Count Words.](../exercises/collections-count-words.md)
    - [Collections. Special Collections.](../exercises/collections-special-collections.md)
- Algorithms And Data Structures
    - [Flood Fill](../exercises/flood-fill.md)
    - [BST Pretty Print](../exercises/bst-pretty-print.md)
    - [Hashtable Open 8-16](../exercises/hashtable-open-8-16.md)
- Object-Oriented Software Design And Programming
    - [Design Patterns](../exercises/design-patterns.md)
- IO
    - [File Tree](../exercises/file-tree.md)
- JUnit
    - [Test Sorting](../exercises/test-sorting.md)
    - [Test Quadratic Equation](../exercises/test-quadratic-equation.md)
    - [Test Factorial](../exercises/test-factorial.md)
- SQL + JDBC.
    - [Employees. SQL Queries.](../exercises/sql-queries.md)
    - [Employees. Row Mapper.](../exercises/row-mapper.md)
    - [Employees. Set Mapper.](../exercises/set-mapper.md)
- Java Streams
    - [Streams. Count Words.](../exercises/streams-count-words.md)
    - [Streams. Pipelines.](../exercises/streams-pipelines.md)
- Java Concurrency Essentials
    - [Thread Factoring](../exercises/thread-factoring.md)
    - [Concurrent TicTacToe](../exercises/concurrent-tictactoe.md)
- Servlets
    - [Expression Calculator](../exercises/expression-calculator.md)
    - [Stateful Expression Calculator](../exercises/stateful-expression-calculator.md)
    - [JSP Currencies](../exercises/jsp-currencies.md)