# Deep Dive Into Java Development

## Exercises

- [Meet Autocode](../exercises/meet-autocode.md)
- [Electronic Watch](../exercises/electronic-watch.md)
- [Quadratic Equation](../exercises/quadratic-equation.md)
- [Average](../exercises/average.md)
- [Catch'em all](../exercises/catch-em-all.md)
- [Segments](../exercises/segments.md)
- [Triangle](../exercises/triangle.md)
- [Figures](../exercises/figures.md)
- [Figures. Extra Challenge](../exercises/figures-extra-challenge.md)
- [Cycle Swap](../exercises/cycle-swap.md)
- [Spiral](../exercises/spiral.md)
- [Test Sorting](../exercises/test-sorting.md)
- [Test Quadratic Equation](../exercises/test-quadratic-equation.md)
- [Test Factorial](../exercises/test-factorial.md)
- [Suffixing App](../exercises/suffixing-app.md)
