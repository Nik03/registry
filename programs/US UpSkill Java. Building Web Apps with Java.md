# Building Web Apps with Java

## Exercises

- Servlets
  - [Expression Calculator](../exercises/expression-calculator.md)
  - [Stateful Expression Calculator](../exercises/stateful-expression-calculator.md)
  - [JSP Currencies](../exercises/jsp-currencies.md)
- SQL and JDBC. Employees Tasks Set.
  - [Employees. SQL Queries.](../exercises/sql-queries.md)
  - [Employees. Row Mapper.](../exercises/row-mapper.md)
  - [Employees. Set Mapper.](../exercises/set-mapper.md)
  - [Employees. DAO.](../exercises/dao.md)
  - [Employees. Service.](../exercises/service.md)
