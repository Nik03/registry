# RD Lab Java. SQL & JDBC

## Exercises

- Meet Autocode
    - [Meet Autocode](../exercises/meet-autocode.md)
- SQL & JDBC
    - [Employees. SQL Queries.](../exercises/sql-queries.md)
    - [Employees. Row Mapper.](../exercises/row-mapper.md)
    - [Employees. Set Mapper.](../exercises/set-mapper.md)
    - [Employees. DAO.](../exercises/dao.md)
    - [Employees. Service.](../exercises/service.md)
