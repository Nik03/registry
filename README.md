# Autocode Exercises and Programs Registry

## Exercises
[Exercises Index](index.md) 

## Programs
### US UpSkill Java
- [US UpSkill Java. Deep Dive Into Java Development](programs/US%20UpSkill%20Java.%20Deep%20Dive%20Into%20Java%20Development.md)
- [US UpSkill Java. Professional Java SE Development](programs/US%20UpSkill%20Java.%20Professional%20Java%20SE%20Development.md)
- [US UpSkill Java. Building Web Apps with Java](programs/US%20UpSkill%20Java.%20Building%20Web%20Apps%20with%20Java.md)
- [US UpSkill Java. Modern Professional Java Web Development with Spring](programs/US%20UpSkill%20Java.%20Modern%20Professional%20Java%20Web%20Development%20with%20Spring.md)

### ITHUB
- [ITHUB Java. Java Extended Mentoring Program](programs/ITHUB%20Java.%20Java%20Extended%20Mentoring%20Program.md)
- [ITHUB Java. Java Web Mentoring Program](programs/ITHUB%20Java.%20Java%20Web%20Mentoring%20Program.md)
- [ITHUB Java, SQL and JDBC Mentoring Program](programs/ITHUB%20Java.%20SQL%20and%20JDBC%20Mentoring%20Program.md)
- [ITHUB Java. Spring Mentoring Program](programs/ITHUB%20Java.%20Spring%20Mentoring%20Program.md)

### RD Trainings
- [RD Java Basics Source Course](programs/RD%20Java%20Basics%20Source%20Course.md)
- [RD Training Java. Standard Program SPB](programs/RD%20Training%20Java.%20Standard%20Program%20SPB.md)
- [RD Training Java. Standard Program SPB v2021-09-02](programs/RD%20Training%20Java.%20Standard%20Program%20SPB%20v2021-09-02.md)
- [RD Training Test Auto Java. Standard Program SPB](programs/RD%20Training%20Test%20Auto%20Java.%20Standard%20Program%20SPB.md)

### RD Lab Courses
- [RD Lab Java. Java 8](programs/RD%20Lab%20Java.%20Java%208.md)
- [RD Lab Java. Spring](programs/RD%20Lab%20Java.%20Spring.md)
- [RD Lab Java. SQL & JDBC](programs/RD%20Lab%20Java.%20SQL%20&%20JDBC.md)

## Guides
- [Exercise Development Guide](guides/exercise-development-guide.md)
- [Test Gems](guides/test-gems.md)