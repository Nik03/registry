# Test Gems

## Hello Autocode

### Task

```java
public class HelloAutocode {
    public static void main(String[] args) {
        //Write a program, printing "Hello, Autocode!"
    }
}
```

### Test Snippet

Have to substitute the original System.out and handle it with care.

```java
public class HelloAutocodeTest {

    @Test
    public void test() {

        final ByteArrayOutputStream sink = new ByteArrayOutputStream();
        PrintStream controlledOut = new PrintStream(sink);
        PrintStream defaultOut = System.out;

        System.setOut(controlledOut);

        try {
            HelloAutocode.main(new String[]{});
            controlledOut.flush();
            final String actual = sink.toString().trim();
            assertEquals("Hello, Autocode!", actual, "Your program must print \"Hello, Autocode!\" but printed \"" + actual + "\" instead.");
        } finally {
            System.setOut(defaultOut);
        }
    }
}
```
---
## Flood Fill

### Task

Implement FloodFill:

- `map` is a String 2D Map of LAND and WATER Cells.
- Flooding iteration converts the neighbour LAND cells of WATER cells into WATER.
- `flood` method must call itself recursively and call `FloodLogger#log` method on each iteration.
- Flooding must complete when all the LAND becomes WATER.

```java
public interface FloodFill {
    void flood(final String map, final FloodLogger logger);
}
```

### Test Snippet

How to check if `flood` method is recursive?

Access the call stack via StackWalker API and count subsequent `flood` method calls each time it logs to `FloodLogger`:

- We may consider a solution to be recursive, if it collects a row like 1,2,3,4...
- We may consider a solution to be non-recursive, if it collects a row like 1,1,1,1...

```java
public class RecursionCountingFloodLoggerImpl extends FloodLoggerImpl {

    List<String> log = new ArrayList<>();
    List<Long> floodMethodChainCounts = new ArrayList<>();

    @Override
    public void log(final String floodState) {
        log.add(floodState);

        final Long floodMethodChainCount = StackWalker.getInstance(StackWalker.Option.RETAIN_CLASS_REFERENCE)
                .walk(frames -> frames
                        .dropWhile(frame -> !isFloodMethod(frame))
                        .takeWhile(frame -> isFloodMethod(frame))
                        .count());

        floodMethodChainCounts.add(floodMethodChainCounts);
    }

    private boolean isFloodMethod(final StackWalker.StackFrame frame) {
        return frame.getMethodName().equals("flood")
                && frame.getMethodType().parameterList().equals(List.of(String.class, FloodLogger.class));
    }
}
```
---
## Suffixing app

### Task

The purpose of this exercise is to train students to design and package Java Applications, basing only on functional
requirements. To complete the exercise a student has to design an application and package it to a runnable jar archive.

### Test Snippet

How to check such a runnable jar having only JUnit framework?

First, we may add student's jar as a system Maven dependency:

```xml

<dependency>
    <groupId>suffixing</groupId>
    <artifactId>suffixing</artifactId>
    <version>1.0</version>
    <scope>system</scope>
    <systemPath>${project.basedir}/src/main/resources/suffixing.jar</systemPath>
</dependency>
```

Then, in tests, we may get the jar manifest and extract the name of the Main class:
```java
...
JarInputStream jarStream = new JarInputStream(new FileInputStream("src/main/resources/suffixing.jar"));
Manifest mf = jarStream.getManifest();
String mainClassName = mf.getMainAttributes().getValue("Main-Class");
Class<?> mainClass = Class.forName(mainClassName);
mainMethod = mainClass.getMethod("main",String[].class);
...
```

Having the name of the Main class, we may invoke its `main` method, passing a needed argument.
```java
...
mainMethod.invoke(null, (Object) new String[]{configFilePath});
...
```

Then we can check if results of program run are the same as we expect.

---
## Collections. Count Words

### Task 
A student need to count words in a huge text and count statistics.
The special requirement is that a solution must not use streams or lambdas.

### Test snippet
A simple and straightforward solution is to get all the source files and check their text:
```java
@Test
public void testNoLambdas() throws IOException {
    Files.walk(Paths.get("src/main/java"))
            .filter(Files::isRegularFile)
            .filter(p -> p.toString().endsWith(".java"))
            .forEach(sourcePath -> {
                String result;
                try {
                    result = String.join("\n", readAllLines(sourcePath, StandardCharsets.UTF_8));
                } catch (IOException e) {
                    throw new IllegalStateException(e);
                }
                final String source = result;

                assertFalse(source.contains("->"));
                assertFalse(source.contains("::"));
                assertFalse(source.contains("stream"));
            });
}
```
---
## Employees. Row Mapper.

### Task
A student must provide an implementation of RowMapper interface to build Employee objects from a result set row.

```java
import java.sql.ResultSet;

public interface RowMapper<T> {
    T mapRow(ResultSet resultSet);
}
```

### Test Snippet
The regular solution is to use H2 Database. It allows to setup a database in-memory.

```xml
<dependency>
    <groupId>org.hsqldb</groupId>
    <artifactId>hsqldb</artifactId>
    <version>2.5.0</version>
    <scope>compile</scope>
</dependency>
```

Then  we can use via regular JDBC API to init a database and provide DDL & DML. 
```java
public class ConnectionSource {
    private static final String JDBC_DRIVER = "org.hsqldb.jdbc.JDBCDriver";
    private static final String DB_URL = "jdbc:hsqldb:mem:myDb";
    private static final String USER = "sa";
    private static final String PASS = "sa";

    ConnectionSource() {
        try {
            DriverManager.registerDriver(new org.hsqldb.jdbc.JDBCDriver());
            try (Connection conn = DriverManager.getConnection(DB_URL, USER, PASS)) {
                try (Statement statement = conn.createStatement()) {
                    statement.execute(getSql("init-ddl.sql"));
                }
                try (Statement statement = conn.createStatement()) {
                    statement.execute(getSql("init-dml.sql"));
                }
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}
```
---

## Expression Calculator Servlet

### Task
Create a servlet, serving expression value calculating.

### Test Snippet
How to check servlets?

Use embedded Tomcat:
```xml
<dependency>
    <groupId>org.apache.tomcat.embed</groupId>
    <artifactId>tomcat-embed-jasper</artifactId>
    <version>8.0.47</version>
</dependency>
```

Then we may init Tomcat programmatically before tests and stop afterwards:
```java
@BeforeAll
public static void startServer() throws ServletException, LifecycleException {

    int port = 8080;

    tomcat = new Tomcat();
    tomcat.setPort(port);

    String webappDirLocation = "src/main/webapp/";
    StandardContext ctx = (StandardContext) tomcat.addWebapp(
            "", new File(webappDirLocation).getAbsolutePath()
    );

    // Declare an alternative location for your "WEB-INF/classes" dir
    // Servlet 3.0 annotation will work

    File additionWebInfClasses = new File("target/classes");
    WebResourceRoot resources = new StandardRoot(ctx);
    resources.addPreResources(
            new DirResourceSet(
                    resources,
                    "/WEB-INF/classes",
                    additionWebInfClasses.getAbsolutePath(), "/"));
    ctx.setResources(resources);

    tomcat.start();
}

@AfterAll
public static void stopServer() throws LifecycleException {
    tomcat.stop();
}

```

So, we can create HTTP requests to a local tomcat and check the result.

---

## Spring Catalog Access

### Task
A student must configure Spring Security to manage access to a Spring Web App endpoints.

### Test Snippet
It is way easier to test Spring Web Apps than Servlet-based ones.
It is due to Spring test frameworks.
In particular, we can use MVC Tests to check access to Spring MVC endpoints, even when you have to specify a logged user:
```java
@ExtendWith(SpringExtension.class)
@SpringBootTest(
        webEnvironment = SpringBootTest.WebEnvironment.MOCK,
        classes = {MvcApplication.class, WebSecurityConfig.class})
@AutoConfigureMockMvc
class MvcTests {

    @Autowired
    private MockMvc mockMvc;

    private static RequestPostProcessor manager() {
        return user("eddie").roles("MANAGER");
    }

    private static RequestPostProcessor customer() {
        return user("bobby").roles("CUSTOMER");
    }

    private static RequestPostProcessor employee() {
        return user("barry").roles("EMPLOYEE");
    }

    @Test
    void testSalaries() throws Exception {
        mockMvc.perform(get("/salaries").with(manager()))
                .andDo(log())
                .andExpect(status().isOk()) //Managers may access all the salaries 
                .andReturn();
        mockMvc.perform(get("/salaries").with(employee()))
                .andDo(log())
                .andExpect(status().isForbidden()) //Employees may NOT access all the salaries
                .andReturn();
        mockMvc.perform(get("/salaries").with(customer()))
                .andDo(log())
                .andExpect(status().isForbidden()) //Customers may NOT access all the salaries
                .andReturn();

        mockMvc.perform(get("/salaries/my").with(manager()))
                .andDo(log())
                .andExpect(status().isOk()) //A manager may access his own salary
                .andReturn();
        mockMvc.perform(get("/salaries/my").with(employee()))
                .andDo(log())
                .andExpect(status().isOk()) //An employee may access his own salary
                .andReturn();
        mockMvc.perform(get("/salaries/my").with(customer()))
                .andDo(log())
                .andExpect(status().isForbidden()) //A customer may NOT access his salary, since he has none
                .andReturn();
    }
}
```

---